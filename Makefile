#!/usr/bin/make -f

CFLAGS += -D_GNU_SOURCE -g -O3

CFLAGS += $(shell pkg-config --cflags libuv)
CFLAGS += $(shell pkg-config --cflags libsystemd)
LDFLAGS += $(shell pkg-config --libs libuv)
LDFLAGS += $(shell pkg-config --libs libsystemd)

all: hddemux hddemux.1

check: hddemux
	PATH=.:$$PATH ./testsuite

hddemux: hddemux.c
	gcc $(CPPFLAGS) $(CFLAGS) $< -Wl,--as-needed $(LDFLAGS) -std=c11 -pedantic -Wall -Werror -o $@

hddemux.1: hddemux.1.md
	pandoc -s -f markdown -t man -o $@ $<

draft-dkg-dprive-demux-dns-http.xml: draft-dkg-dprive-demux-dns-http.md
	kramdown-rfc2629 < $< > $@

draft-dkg-dprive-demux-dns-http.html: draft-dkg-dprive-demux-dns-http.xml
	xml2rfc $< --html

draft-dkg-dprive-demux-dns-http.txt: draft-dkg-dprive-demux-dns-http.xml
	xml2rfc $< --text

clean:
	rm -rf hddemux hddemux.1 \
	.refcache/ \
	draft-dkg-dprive-demux-dns-http.txt \
	draft-dkg-dprive-demux-dns-http.html \
	draft-dkg-dprive-demux-dns-http.xml

.PHONY: clean all check
